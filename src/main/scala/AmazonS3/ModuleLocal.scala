package AmazonS3

import scala.io.Source
import scala.collection.JavaConversions._
import com.amazonaws.AmazonClientException
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.auth.{AWSCredentials, AWSStaticCredentialsProvider}
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.model.{Bucket, ListObjectsRequest, ObjectListing}
import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}

import java.util.UUID
import scala.collection.mutable.ListBuffer
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.Writer
import java.util.UUID
import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectSummary

import scala.sys.SystemProperties.headless.key


//object ModuleLocal {
//  def main(args: Array[String]): Unit = {
//
//    val credentials = new ProfileCredentialsProvider().getCredentials
//    val s3 = AmazonS3ClientBuilder.standard
//      .withCredentials(new AWSStaticCredentialsProvider(credentials))
//      .withEndpointConfiguration(new AwsClientBuilder
//      .EndpointConfiguration("storage.yandexcloud.net", "ru-central1"))
//      .build
//    val S3_bucketName: String = "test-gpm-data-2"
//
//    //      println("Creating bucket " + bucketName + "\n")
//    ////      s3.createBucket(bucketName)
//
//    //     println("List the buckets in your account")
//    //    val lines = s3.listBuckets().toArray.toList
//    //    var lines_buf = new ListBuffer[String]()
//    //    for (l_n <- lines){lines_buf.+=(l_n.toString.replace("S3Bucket [name=","")
//    //      .replaceAll(", creationDate=.*owner=null]", ""))}
//    //    val  lines_3 = lines_buf.toList
//    //    println(lines_3)
//
//
//
//    System.out.println("Uploading a new object to S3 from a file\n")
//    s3.putObject(new PutObjectRequest(S3_bucket, "", "df_all_26nov.parquet"))
//
//
////
////
////    println("Listing objects in bucket replace parts*** and save to list")
////    val objectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix("s3"))
////    var obj_buf = new ListBuffer[String]()
////    for (objectSummary <- objectListing.getObjectSummaries) {
////      obj_buf.+=(objectSummary.getKey.replaceAll("part-00.*", "")
////        .replaceAll("_spark_metadata.*", "").replaceAll("s3", "s3a://test-gpm-data/s3"))}
////
////    val  obj_list = obj_buf.toList.distinct
////    println(obj_list)
////
////
////
////    //    val lines_2 = lines.map(_.replace("DIR  s3","s3a"))
////    //    println(lines_2)
////    //
////    //    val lines_3 = lines_2.filter( name => name.contains("s3a://test-gpm-data/s3"))
////    //    //    println(lines_3)
////
////
////    //      for (buket_x <- s3.listBuckets()) {
////    //        println(buket_x)
////    //      }
////
////
////    //  //  val proj = "Tchaykovsky_15nov"
////    //  //  val path_prqt = "df-kaf-ydx-15nov"
////    //  //  val S3_bucket = "test-gpm-data"
////    //  //  val kafk_parqt = s"/Users/pmurzakov/IdeaProjects/$proj/$path_prqt"
////    //  //  val S3_parquet = s"${path_prqt}-s3"
////    //  //
////    //  def main(args: Array[String]) {
////    //    val kaf_yandx = new Kafk_Parq_Yandx
////    //    //        kaf_yandx.s3_csv_write
////    //    //        kaf_yandx.read_kfk_wrt_prqt()
////    //    ////        kaf_yandx.read_parq()
////    //    //    //    kaf_yandx.push_s3()
////    //    kaf_yandx.read_parq_s3()
////    //
//  }
//  //  }
//}
//
