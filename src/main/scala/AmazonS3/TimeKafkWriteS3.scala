package AmazonS3

import KafkWriteParqS3.KafkWriteParqS3

import java.time.LocalDateTime
import java.util.{Date, Timer, TimerTask}

object TimeKafkWriteS3 extends App {

    val timerTask = new MyTimerTask // стартуем TimerTask в виде демона
    val timer = new Timer(true)

    // loop restart read/write in ХХ sec
    timer.scheduleAtFixedRate(timerTask, 0, 5 * 1000) // 80 minutes + 8 sec
    System.out.println("TimerTask начал выполнение")

    try Thread.sleep(50 * 1000) // call cancel() in XX sec
    catch {case e: InterruptedException => e.printStackTrace() }

    timer.cancel()
    System.out.println("TimerTask прекращена")
    try Thread.sleep(0 * 1000)
    catch { case e: InterruptedException => e.printStackTrace() }
}

class MyTimerTask extends TimerTask {

  val kafkWriteParqS3 = new KafkWriteParqS3
//  val kafkWriteJsonS3 = new KafkWriteJsonS3

  override def run(): Unit = {
    System.out.println("TimerTask start read/write in : " + new Date)
    completeTask()
    System.out.println("TimerTask finish read/write in : " + new Date)
  }

  private def completeTask(): Unit = {
    var tmStmp = LocalDateTime.now()
    try
      kafkWriteParqS3.read_kfk_wrt_prqt(tmStmp)
//      kafkWriteJsonS3.read_kfk_wrt_json(tmStmp)
    catch { case e: InterruptedException => e.printStackTrace()}
  }
}





