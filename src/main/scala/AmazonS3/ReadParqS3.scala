package AmazonS3

import SessionAndConnections.AppConfig
import SessionAndConnections.SparkSess.spark
import SparkReadS3.S3ObjectList
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class ReadParqS3 {
//  val loger_2 = Logger.getLogger(this.getClass.getName)

  def read_parq() = {

//    //    println("Запускаем скрипт - сохраняем пути бакетов S3 в txt файл")
//    //    "src/main/resources/script_s3_paths_to_file.sh" !!
//    //    println("Из файла сохраняем пути банкетов в лист и заменяем DIR_s3 на s3a, отфильтруем checkpnt")
//    //    val lines = Source.fromFile("/Users/pmurzakov/IdeaProjects/Tchaykovsky_15nov/paths_222_333.txt").getLines.toList
//
    //  "Вставить реально существующие пути к файлам паркет в S3")

////    DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:21:34.284061/
////      DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:33:09.418842/
////    DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:33:43.514951/
//
//    val df_1 = spark.read.parquet(s"s3a://test-gpm-data-30nov/s3-2021-11-30T20:43:25.447640/part-00000-21c00cf3-a9c7-4471-afcf-d761c94a4d91-c000.snappy.parquet")
////    val df_2 = spark.read.parquet(s"s3a://test-gpm-data-30nov-2/s3-2021-11-30T18:33:09.418842/")
////    val df_3 = spark.read.parquet(s"s3a://test-gpm-data-30nov-2/s3-2021-11-30T18:33:43.514951/")
////    val df_4 = spark.read.parquet(s"s3a://test-gpm-data-30nov/s3-2021-11-30T15:36:08.469689/")
////    val df_5 = spark.read.parquet(s"s3a://test-gpm-data-30nov/s3-2021-11-30T15:56:14.635596/")
//
//
//    val df_union = df_1  //.union(df_2).union(df_3)  //.union(df_4).union(df_5)
//    val df_unon_distnct = df_union.distinct().sort("offset")
//
////    df_union.show(100)
////    println("df_unon_distnct.show(200)")
//    df_unon_distnct.show(200)
//    println("df_union.count()", df_union.count())
//    println("df_unon_distnct.count()", df_unon_distnct.count())
//    df_union.printSchema()
//    df_union.describe("offset").show()
        println("Стартуем спарк сессию")

        val S3_bucket = AppConfig("S3_bucket")
        val buck_union_ext = AppConfig("buck_union_ext")
        println("Получаем лист объектов S3//bucket/     (из импортируемого класса)")
        val s3ObjectList = new S3ObjectList
        val listObjects = s3ObjectList.s3ObjectsToList()

        println("Заменяем часть строки  и фильтруем")
        val lines_2 = listObjects.map(_.replace("                          DIR  s3", "s3a"))
        val s3pathList = lines_2.filter(name => name.contains(s"s3a://$S3_bucket/s3"))
        println(lines_2)
        println(s3pathList)

        println("В цикле читаем все паркеты из сохранённых путей S3 и записываем в Другой Bucket")
        val nn = s3pathList.length

    for (n <- 0 until (nn)) {
      spark.read.parquet(s3pathList(n)).show()

    }

        for (n <- 0 until (nn)) {
          println(spark.read.parquet(s3pathList(n))
            .write.mode("ignore").parquet(s"s3a://$S3_bucket/$buck_union_ext/"))
        }

        println("Читаем объединенный паркет из Другого Bucketa")
        val df_all = spark.read.parquet(s"s3a://$S3_bucket/$buck_union_ext").distinct().sort("partition", "offset")
        df_all.show(500)
        println("df_all.count() - ", df_all.count())


        println("Выделяем колонку log и timestamp и записываем DF")
        val df_union_parse = df_all.select("log", "timestamp")
    //    df_union_parse.write.parquet("/Users/pmurzakov/IdeaProjects/Tchaykovsky_15nov/df_union_pars_27nov.parquet")

    //    println("читаем записаннный DF")
    //    val df_union_log_r = spark.read.parquet("/Users/pmurzakov/IdeaProjects/Tchaykovsky_15nov/df_union_pars_27nov.parquet")

        df_union_parse.show(50)
        df_union_parse.printSchema()

        println("Из колонки log выделяем колонки asctime and message")
        val schema = new StructType()
          .add("asctime", StringType, true)
          .add("message", StringType, true)

        val df4 = df_union_parse.withColumn("log", from_json(col("log"), schema))
        df4.printSchema()
        df4.show()

        val df5 = df4.select(col("timestamp"), col("log.*"))
        df5.show()
        df5.printSchema()

        //    println("Из колонки message Разделяем текст по запятым и превращаем в массив")
        //    val df5_2 = df5.select(split(col("message"),",").as("messageArray"))   //.drop("name")
        //    df5_2.printSchema()
        //    df5_2.show()

        //    println("Из колонки message Разделяем текст по запятым и разносим в отдельные колонки")
        //    val splitDF = df5
        //      .withColumn("col_0",split(col("message"),",").getItem(0))
        //      .withColumn("col_1",split(col("message"),",").getItem(1))
        //      .withColumn("col_2",split(col("message"),",").getItem(2))
        //      .withColumn("col_3",split(col("message"),",").getItem(3))
        //      .withColumn("col_4",split(col("message"),",").getItem(4))
        //      .withColumn("col_5",split(col("message"),",").getItem(5))
        //      .withColumn("col_6",split(col("message"),",").getItem(6))
        //      .withColumn("col_7",split(col("message"),",").getItem(7))
        //      .withColumn("col_8",split(col("message"),",").getItem(8))
        //      .withColumn("col_9",split(col("message"),",").getItem(9))
        //      .withColumn("col_10",split(col("message"),",").getItem(10))
        //      .withColumn("col_11",split(col("message"),",").getItem(11))
        //      .withColumn("col_12",split(col("message"),",").getItem(12))
        //      .withColumn("col_13",split(col("message"),",").getItem(13))
        //      .withColumn("col_14",split(col("message"),",").getItem(14))
        //      .withColumn("col_15",split(col("message"),",").getItem(15))
        //      .withColumn("col_16",split(col("message"),",").getItem(16))
        //      .withColumn("col_17",split(col("message"),",").getItem(17))
        //      .withColumn("col_18",split(col("message"),",").getItem(18))
        //      .withColumn("col_19",split(col("message"),",").getItem(19))
        //      .withColumn("col_20",split(col("message"),",").getItem(20))
        //      .withColumn("col_21",split(col("message"),",").getItem(21))
        //      .withColumn("col_22",split(col("message"),",").getItem(22))
        //      .withColumn("col_23",split(col("message"),",").getItem(23))
        //      .withColumn("col_24",split(col("message"),",").getItem(24))
        //      .withColumn("col_25",split(col("message"),",").getItem(25))
        //      .withColumn("col_26",split(col("message"),",").getItem(26))
        //      .withColumn("col_27",split(col("message"),",").getItem(27))
        //      .withColumn("col_28",split(col("message"),",").getItem(28))
        //      .withColumn("col_29",split(col("message"),",").getItem(29))
        //      .withColumn("col_30",split(col("message"),",").getItem(30))
        //      .withColumn("col_31",split(col("message"),",").getItem(31))
        //      .withColumn("col_32",split(col("message"),",").getItem(32))
        //      .withColumn("col_33",split(col("message"),",").getItem(33))
        //      .withColumn("col_34",split(col("message"),",").getItem(34))
        //      .withColumn("col_35",split(col("message"),",").getItem(35))
        //      .withColumn("col_36",split(col("message"),",").getItem(36))
        //      .withColumn("col_37",split(col("message"),",").getItem(37))
        //      .withColumn("col_38",split(col("message"),",").getItem(38))
        //      .withColumn("col_39",split(col("message"),",").getItem(39))
        //      .withColumn("col_40",split(col("message"),",").getItem(40))
        //      .withColumn("col_41",split(col("message"),",").getItem(41))
        //      .withColumn("col_42",split(col("message"),",").getItem(42))
        //      .withColumn("col_43",split(col("message"),",").getItem(43))
        //      .withColumn("col_44",split(col("message"),",").getItem(44))
        //      .withColumn("col_45",split(col("message"),",").getItem(45))
        //      .withColumn("col_46",split(col("message"),",").getItem(46))
        //      .withColumn("col_47",split(col("message"),",").getItem(47))
        //      .withColumn("col_48",split(col("message"),",").getItem(48))
        //      .withColumn("col_49",split(col("message"),",").getItem(49))
        //      .withColumn("col_50",split(col("message"),",").getItem(50))
        //      .withColumn("col_51",split(col("message"),",").getItem(51))
        //      .withColumn("col_52",split(col("message"),",").getItem(52))
        //      .withColumn("col_53",split(col("message"),",").getItem(53))
        //      .withColumn("col_54",split(col("message"),",").getItem(54))
        //      .withColumn("col_55",split(col("message"),",").getItem(55))
        //      .withColumn("col_56",split(col("message"),",").getItem(56))
        //      .withColumn("col_57",split(col("message"),",").getItem(57))
        //      .withColumn("col_58",split(col("message"),",").getItem(58))
        //      .withColumn("col_59",split(col("message"),",").getItem(59))
        //      .withColumn("col_60",split(col("message"),",").getItem(60))
        //      .withColumn("col_61",split(col("message"),",").getItem(61))
        //      .withColumn("col_62",split(col("message"),",").getItem(62))
        //      .withColumn("col_63",split(col("message"),",").getItem(63))
        //      .withColumn("col_64",split(col("message"),",").getItem(64))
        //      .withColumn("col_65",split(col("message"),",").getItem(65))
        //      .withColumn("col_66",split(col("message"),",").getItem(66))
        //      .withColumn("col_67",split(col("message"),",").getItem(67))
        //      .withColumn("col_68",split(col("message"),",").getItem(68))
        //      .withColumn("col_69",split(col("message"),",").getItem(69))
        //      .withColumn("col_70",split(col("message"),",").getItem(70))
        //      .withColumn("col_71",split(col("message"),",").getItem(71))
        //      .withColumn("col_72",split(col("message"),",").getItem(72))
        //      .withColumn("col_73",split(col("message"),",").getItem(73))
        //      .withColumn("col_74",split(col("message"),",").getItem(74))
        //      .withColumn("col_75",split(col("message"),",").getItem(75))
        //      .withColumn("col_76",split(col("message"),",").getItem(76))
        //      .withColumn("col_77",split(col("message"),",").getItem(77))
        //      .withColumn("col_78",split(col("message"),",").getItem(78))
        //      .withColumn("col_79",split(col("message"),",").getItem(79))
        //      .withColumn("col_80",split(col("message"),",").getItem(80))
        //      .withColumn("col_81",split(col("message"),",").getItem(81))
        //      .withColumn("col_82",split(col("message"),",").getItem(82))
        //      .withColumn("col_83",split(col("message"),",").getItem(83))
        //      .withColumn("col_84",split(col("message"),",").getItem(84))
        //      .withColumn("col_85",split(col("message"),",").getItem(85))
        //      .withColumn("col_86",split(col("message"),",").getItem(86))
        //      .withColumn("col_87",split(col("message"),",").getItem(87))
        //      .withColumn("col_88",split(col("message"),",").getItem(88))
        //      .withColumn("col_89",split(col("message"),",").getItem(89))
        //      .drop("message")

        println("Из колонки message Разделяем текст по ***Response ready. body=  - во 2й кол получаем json")
        val splitDF_redBody = df5
          .withColumn("col_0", split(col("message"), "Response ready. body=").getItem(0))
          .withColumn("col_1", split(col("message"), "Response ready. body=").getItem(1))
          .drop("message")

        splitDF_redBody.show(false)
        splitDF_redBody.printSchema()

        println("Из JSON  колонки col_1 выделяем вложенные колонки ...")
        val schema_3 = new StructType().add("blocks", StringType, true)

        val jsonSplit_2 = splitDF_redBody.withColumn("col_1", from_json(col("col_1"), schema_3))
        jsonSplit_2.printSchema()
        jsonSplit_2.show()

        val jsonSplit_3 = jsonSplit_2.select(col("timestamp"), col("asctime"), col("col_1.*"))
        jsonSplit_3.show(100)
        jsonSplit_3.printSchema()


        //    val splitDfFiltr = splitDF.filter(col("col_0").rlike("(?i)^Response*") && col("col_5").rlike("(?i)^[a-z]*"))
        //    splitDfFiltr.show(false)
        //    splitDfFiltr.printSchema()


        //    val df5_2=df5.withColumn("message",from_json(col("message"),MapType(StringType,StringType)))
        //    df5_2.show(false)
        //    df5_2.printSchema()

        //    val schema_2 = new StructType()
        //      .add("blocks", StringType, true)
        //
        //    val df6 = df5.withColumn("message", from_json(col("message"), schema_2))
        //    df6.printSchema()
        //    df6.show()
        //
        //    val df7 = df6.select(col("message.*"))
        //    df7.printSchema()
        //    df7.show(100)
  }
}