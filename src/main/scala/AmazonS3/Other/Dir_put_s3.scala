package AmazonS3.Other

import com.amazonaws.{AmazonServiceException, SdkClientException}
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.transfer.TransferManagerBuilder

import java.io.File


class Dir_put_s3 {
  @throws[Exception]
  def put_dir_s3(bucketName:String, filePath:String): Unit = {
    val clientRegion = Regions.DEFAULT_REGION
//    val bucketName = "*** Bucket name ***"
    val keyName = "92b77fsJhAuVmWtGReAT"
//    val filePath = "*** Path for file to upload ***"
    try {
      val s3Client = AmazonS3ClientBuilder.standard.withRegion(clientRegion).withCredentials(new ProfileCredentialsProvider).build
      val tm = TransferManagerBuilder.standard.withS3Client(s3Client).build
      // TransferManager processes all transfers asynchronously,
      // so this call returns immediately.
      val upload = tm.upload(bucketName, keyName, new File(filePath))
      System.out.println("Object upload started")
      // Optionally, wait for the upload to finish before continuing.
      upload.waitForCompletion()
      System.out.println("Object upload complete")
    } catch {
      case e: AmazonServiceException =>
        // The call was transmitted successfully, but Amazon S3 couldn't process
        // it, so it returned an error response.
        e.printStackTrace()
      case e: SdkClientException =>
        // Amazon S3 couldn't be contacted for a response, or the client
        // couldn't parse the response from Amazon S3.
        e.printStackTrace()
    }
  }
}
