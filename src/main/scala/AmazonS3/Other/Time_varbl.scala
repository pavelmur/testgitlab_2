package AmazonS3.Other

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Time_varbl {
  def main(args: Array[String]): Unit = {


    def a() = {
      println(LocalDateTime.now())
      println(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss").format(LocalDateTime.now))

      var tt = LocalDateTime.now()
      var path = s"path/to_file/aa_$tt"
      println(path)
    }
    a()
  }
}
