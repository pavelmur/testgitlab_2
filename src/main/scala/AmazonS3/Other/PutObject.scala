package AmazonS3.Other

import com.amazonaws.AmazonServiceException
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder

import java.io.File
import java.nio.file.Paths

/**
 * Upload a file to an Amazon S3 bucket.
 *
 * This code expects that you have AWS credentials set up per:
 * http://docs.aws.amazon.com/java-sdk/latest/developer-guide/setup-credentials.html
 */
class PutObject {
  def put(bucket_name:String, file_path:String){

    val key_name = Paths.get(file_path).getFileName.toString
    System.out.format("Uploading %s to S3 bucket %s...\n", file_path, bucket_name)
    val s3 = AmazonS3ClientBuilder.standard.withRegion(Regions.DEFAULT_REGION).build
    try s3.putObject(bucket_name, key_name, new File(file_path))
    catch {
      case e: AmazonServiceException =>
        System.err.println(e.getErrorMessage)
        System.exit(1)
    }
    System.out.println("Done!")
  }
}
