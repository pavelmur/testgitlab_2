package AmazonS3.Other.Other.Tchay

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import java.util.Properties
import scala.sys.process._

class Kafk_Parq_Yandx {
    val path_prqt = "df-kaf-ydx-15nov"
    val S3_bucket = "test-gpm-data"
    val kafk_parqt = s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/$path_prqt"
    val S3_parquet = s"${path_prqt}-s3"

    println("Стартуем спарк сессию")
    val spark = SparkSession
      .builder
      .appName("Other.Tchay.Kafk_Parq_Yandx")
      .config("spark.master", "local[*]")
      .getOrCreate()

    val HOST = "rc1b-7stmq5dfi6n4k2lt.mdb.yandexcloud.net:9091"
    val TOPIC = "api-tch-logs"
    val USER = "gpmdata"
    val PASS = "aJeYAnFXByG9"
    val TS_FILE = "/etc/security/ssl"
    val TS_PASS = "Pivanet1"
    val jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";"
    val jaasCfg = String.format(jaasTemplate, USER, PASS)
    val GROUP = "demo"
    val deserializer = classOf[StringDeserializer].getName
    val props  = new Properties

    println("df - читает поток из топика кафки")
    val df = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", HOST)
      .option("subscribe", TOPIC)
//      .option("kafka.auto.offset.reset", "latest")
      .option("kafka.startingOffsets", "latest") //earliest  latest   only for first query
//      .option("startingOffsets", """{"topic1":{"0":23,"1":-2},"topic2":{"0":-2}}""") // only for first query
//      .option("endingOffsets", """{"topic1":{"0":50,"1":-1},"topic2":{"0":-1}}""") // only for batch
      .option("enable.auto.commit", "true")
      .option("kafka.group.id", GROUP)
      .option(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, deserializer)
      .option(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer)
      .option("kafka.security.protocol", "SASL_SSL")
      .option("kafka.sasl.mechanism", "SCRAM-SHA-512")
      .option("kafka.sasl.jaas.config", jaasCfg)
      .option("kafka.ssl.truststore.location", TS_FILE)
      .option("kafka.ssl.truststore.password", TS_PASS)
      .load()

    def read_kfk_wrt_prqt() = {

//        println("Пишем в консоль")
//        df.writeStream.format("console")
//          .outputMode("append")
//          .start()
//          .awaitTermination(15000)

        println("Определяем схему для value")
        val schema = new StructType().add("@timestamp", StringType)
          .add("log", StringType)
          .add("stream", StringType)
          .add("time", TimestampType)

        println("Делаем каст колонок ")
        val personDF = df.select(col("key").cast("string"),
            (from_json(col("value").cast("string"), schema)
              .as("data")), col("partition"),col("offset"), col("timestamp"))
                .select("key", "data.*", "partition", "offset", "timestamp")

        println("Пишем в паркет")
        personDF.writeStream.outputMode("append").format("parquet")
            .option("checkpointLocation", s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/checkpt_${path_prqt}")
            .option("path", s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/${path_prqt}")
//            .option("path", "https://storage.yandexcloud.net/test-gpm-data-2/")
            .start()
            .awaitTermination(17000)

        personDF.printSchema()
    }

    def read_parq() = {
      println("Читаем DF из паркета")

      val df_ydx = spark.read.parquet(s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/$path_prqt")
      df_ydx.show()
      println("df_ydx.count()", df_ydx.count())
      df_ydx.printSchema()
      df_ydx.describe("offset").show()
      df_ydx.describe("timestamp").show()
    }

      def push_s3() = {
        val ls_S3_bucket = s"s3cmd ls s3://$S3_bucket/ !"
        val push_to_S3 = s"s3cmd --storage-class COLD put --recursive $kafk_parqt s3://$S3_bucket/"
        val get_from_s3 = s"s3cmd get --recursive s3://$S3_bucket/$path_prqt"
        val ls_all = "ls -a"

        def ls_S_3() = ls_S3_bucket !
        def push_S3() =  push_to_S3 !

        def get_f_s3() = {println(get_from_s3)
          get_from_s3 !}
        def ls_a() =   ls_all !

//        ls_S_3()
//        println("Переносим файл в S3")
//        push_S3()
//        println("Листинг хранилища S3")
        ls_S_3()
        get_f_s3()
        ls_S_3()
      }

      def read_parq_s3() = {
        println("Читаем DF из паркета")

//        val df_ydx = spark.read.parquet(s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/$S3_parquet")
        val df_ydx = spark.read.parquet(s"/Users/pmurzakov/IdeaProjects/Tchaykovsky/df-kaf-ydx-15nov")
        df_ydx.show()
        println("df_ydx.count()", df_ydx.count())
        df_ydx.printSchema()
        df_ydx.describe("offset").show()
        df_ydx.describe("timestamp").show()
      }

}
