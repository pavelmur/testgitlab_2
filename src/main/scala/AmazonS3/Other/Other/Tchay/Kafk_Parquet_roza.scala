package AmazonS3.Other.Other.Tchay

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.{StringType, StructType}

class Kafk_Parquet_roza {
    println("Стартуем спарк сессию")
    val spark = SparkSession
      .builder
      .appName("Kafk_Parq_Roza")
      .config("spark.master", "local[*]")
      .getOrCreate()

    println("Читаем поток из топика кафки")
    val df = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "kafka20.dw.zxz.su:9092,kafka21.dw.zxz.su:9092,kafka22.dw.zxz.su:9092")
      .option("subscribe", "matchtv_page_view")
      .option("startingOffsets", "latest") //earliest"
      .load()

    println("извлекаем значения, из JSON и форматируем используя настраиваемую схему" +
      "Записываем в паркет")
    def df_string() = {
        df.writeStream
          .format("console")
          .outputMode("append")
          .start()
          .awaitTermination(7000)

        val schema = new StructType()
          .add("uuid", StringType)
          .add("uuid_name", StringType)
          .add("ip", StringType)
          .add("dt", StringType)
          .add("user_agent", StringType)
          .add("log_dt", StringType)

        println("Делаем каст колонок в стринг")
        val personDF = df.select(col("key").cast("string"), (from_json(col("value").cast("string"), schema).as("data"))).select("key", "data.*")

        //    personDF.printSchema()
        personDF.writeStream
        .outputMode("append")
        .format("parquet")
        .outputMode("append")
        .format("parquet")
        .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_kaf_roz_check_3")
        .option("path", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_kaf_roz_3")
        .start()
        .awaitTermination(7000)

        personDF.printSchema()

        println("Читаем датаФрэйм из паркета")
        val df_roz = spark.read.parquet("/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_kaf_roz_3")
        df_roz.show()
        println("df_roz.count()", df_roz.count())
        df_roz.printSchema()


//        val personDF = df.select(col("key").cast("string"), (from_json(col("value").cast("string"), schema).as("data"))).select("key", "data.*")
//        //        personDF.printSchema()
//        personDF.writeStream
//          .outputMode("append")
//          .format("parquet")
//          .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_2_check")
//          .option("path", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_2")
//          .start()
//          .awaitTermination(3000)
//
//        println("Записываем данные в паркет в хранилище")
//        val df_1 = spark.read.parquet("/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_2")
//        df_1.printSchema()
//        df_1.show()

    }

}
