package AmazonS3.Other.Other

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.{FloatType, IntegerType, StringType, StructType}

class Kafk_Parq_Locl {
    println("Стартуем спарк сессию")
    val spark = SparkSession
        .builder
        .appName("Other.Kafk_Parq_Locl")
        .config("spark.master", "local[*]")
        .getOrCreate()

    println("Читаем поток из топика кафки")
    val df = spark.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", "localhost:9092")
        .option("subscribe", "top_9")
        .option("startingOffsets", "earliest")
        .load()

    println("извлекаем значения, из JSON и форматируем используя настраиваемую схему" +
      "Записываем в пакет")
    def df_string() = {
        df.writeStream
          .format("console")
          .outputMode("append")
          .start()
          .awaitTermination(3000)

        val schema = new StructType()
          .add("cust_id", IntegerType)
          .add("month", IntegerType)
          .add("expenses", FloatType)
          .add("names", StringType)

        val personDF = df.select(col("key").cast("string"), (from_json(col("value").cast("string"), schema).as("data"))).select("key", "data.*")
        //        personDF.printSchema()
        personDF.writeStream
          .outputMode("append")
          .format("parquet")
          .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_3_check")
          .option("path", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_3")
          .start()
          .awaitTermination(3000)

        println("Записываем данные в паркет в хранилище")
        val df_1 = spark.read.parquet("/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_9_3")
        df_1.printSchema()
        df_1.show()

//        // Write new data to Parquet files
//        def write_parq() = df.writeStream
//          .outputMode("append")
//          .format("parquet")
//          .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_7_4_check")
//          .option("path", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_7_4")
//          .start()
    }
}



//
//    def write_myTable() = {
//        val my_Table = df.writeStream
//          .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_7_3")
//          .toTable("myTable_7_3")
//
//    }
//
////        write_myTable().createOrReplaceTempView("myTable_7_3")
//
//        // Check the table result
//    def show_myTable() =
//        spark.read.table("myTable_7_3").show()
//
////    // Transform the source dataset and write to a new table
////    def transfrm_myTable() =
////        spark.readStream
////          .table("myTable_7_3")
////          .select("value")
////          .writeStream
////          .option("checkpointLocation", "/Users/pmurzakov/IdeaProjects/ChaykovskTest1/df_7_3")
////          .format("parquet")
////          .toTable("new_Table_7_3")
////
////        // Check the new table result
////        spark.read.table("new_Table_7_3").show()

////
////

