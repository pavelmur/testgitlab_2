package AmazonS3.Other

import java.util.Date
import java.util.Timer
import java.util.TimerTask


object MyTimerTask {
  def main(args: Array[String]): Unit = {

    val timerTask = new MyTimerTask
    // стартуем TimerTask в виде демона
    val timer = new Timer(true)

    // будем запускать каждых 10 секунд (10 * 1000 миллисекунд)
    timer.scheduleAtFixedRate(timerTask, 0, 15 * 1000)

    System.out.println("TimerTask начал выполнение")
    // вызываем cancel() через какое-то время
    try Thread.sleep(1000*25)
    catch {
      case e: InterruptedException =>
        e.printStackTrace()
    }
    timer.cancel()
    System.out.println("TimerTask прекращена")
    try Thread.sleep(10000)
    catch {
      case e: InterruptedException =>
        e.printStackTrace()
    }
  }
}

class MyTimerTask extends TimerTask {
  override def run(): Unit = {
    System.out.println("TimerTask начал свое выполнение в:" + new Date)
    completeTask()
    System.out.println("TimerTask закончил свое выполнение в:" + new Date)
  }

  private def completeTask(): Unit = {
    try // допустим, выполнение займет 20 секунд
      Thread.sleep(4000)
    catch {
      case e: InterruptedException =>
        e.printStackTrace()
    }
  }
}