package AmazonS3.Other

import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer

import java.util
import java.util.Properties

object App_Scala {

  def main(args: Array[String]): Unit = {

    val HOST = "rc1b-7stmq5dfi6n4k2lt.mdb.yandexcloud.net:9091"
    val TOPIC = "api-tch-logs"
    val USER = "gpmdata"
    val PASS = "aJeYAnFXByG9"
    val TS_FILE = "/etc/security/ssl"
    val TS_PASS = "Pivanet1"
    val jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";"
    val jaasCfg = String.format(jaasTemplate, USER, PASS)
    val GROUP = "demo"
    val deserializer = classOf[StringDeserializer].getName
    val props = new Properties

    props.put("bootstrap.servers", HOST)
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    props.put("group.id", GROUP)
    props.put("key.deserializer", deserializer)
    props.put("value.deserializer", deserializer)
    props.put("security.protocol", "SASL_SSL")
    props.put("sasl.mechanism", "SCRAM-SHA-512")
    props.put("sasl.jaas.config", jaasCfg)
    props.put("ssl.truststore.location", TS_FILE)
    props.put("ssl.truststore.password", TS_PASS)

    val consumer = new KafkaConsumer[String, String](props)
    consumer.subscribe(util.Collections.singletonList(TOPIC))

    while ( {
      true
    }) {
      val records = consumer.poll(100)
      import scala.collection.JavaConversions._
      for (record <- records) {
        System.out.println(record.key + ":" + record.value)
      }
    }
  }
}
