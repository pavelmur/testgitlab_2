//package AmazonS3.Other
//
//import com.amazonaws.AmazonClientException
//import com.amazonaws.auth.{AWSCredentials, AWSStaticCredentialsProvider}
//import com.amazonaws.auth.profile.ProfileCredentialsProvider
//import com.amazonaws.services.s3.AmazonS3ClientBuilder
//
//class S3_test_class {
//
//  var credentials: AWSCredentials = null
//  try credentials = new ProfileCredentialsProvider().getCredentials
//  catch {
//    case e: Exception =>
//      throw new AmazonClientException("Cannot load the credentials from the credential profiles file. " + "Please make sure that your credentials file is at the correct " + "location (~/.aws/credentials), and is in valid format.", e)
//  }
//
//  val s3 = AmazonS3ClientBuilder.standard()
//    .withCredentials(new AWSStaticCredentialsProvider(credentials))
//    .withEndpointConfiguration(
//      new AmazonS3ClientBuilder.EndpointConfiguration(
//        "storage.yandexcloud.net","ru-central1"
//      )
//    )
//    .build();
//
//}
