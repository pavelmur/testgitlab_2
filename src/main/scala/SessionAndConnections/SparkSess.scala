package SessionAndConnections

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

object SparkSess {

  val logger = Logger.getLogger(this.getClass)
  logger.debug("start spark = SparkSession")

  val spark = SparkSession
    .builder
    .appName(AppConfig("appName"))
    .config("spark.master", AppConfig("sparkMaster"))
    .config("spark.hadoop.fs.s3a.endpoint", AppConfig("s3aEndpoint"))
    .config("spark.hadoop.fs.s3a.access.key", AppConfig("s3aAccessKey"))
    .config("spark.hadoop.fs.s3a.secret.key", AppConfig("s3aSecretKey"))
    .config("spark.hadoop.fs.s3a.path.style.access", AppConfig("s3aPathStyleAccess"))
    .config("spark.hadoop.fs.s3a.impl", AppConfig("s3aImpl"))
    .getOrCreate()

}
