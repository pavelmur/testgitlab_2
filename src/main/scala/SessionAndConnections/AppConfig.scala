package SessionAndConnections

import java.io.FileInputStream
import java.util.Properties

object AppConfig {

  private val prop = new Properties();
  prop.load(new FileInputStream("/Users/pmurzakov/IdeaProjects/Tchaykovsky_15nov/src/main/resources/app.properties"));

  def apply(s: String) = prop.getProperty(s)
}
