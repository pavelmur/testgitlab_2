package KafkWriteParqS3

import SessionAndConnections.AppConfig
import SessionAndConnections.SparkSess.spark
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import java.time.LocalDateTime
import java.util.Properties
import scala.sys.process._
import org.apache.log4j.{LogManager, Logger}

class KafkWriteParqS3 {
  //val loger_2 = Logger.getLogger(this.getClass.getName)
  //  val proj = "Tchaykovsky_15nov"
  //  val path_prqt = "df-kaf-ydx-15nov"
  //  val S3_bucket = "test-gpm-data"
  //  val kafk_parqt = s"/Users/pmurzakov/IdeaProjects/$proj/$path_prqt"
  //  val S3_parquet = s"${path_prqt}-s3"
  //  val HOST = "rc1b-7stmq5dfi6n4k2lt.mdb.yandexcloud.net:9091"
  //  val TOPIC = "api-tch-logs"
  //  val USER = "gpmdata"
  //  val PASS = "aJeYAnFXByG9"
  //  val TS_FILE = "/etc/security/ssl"
  //  val TS_PASS = "Pivanet1"
  //  val jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";"
  //  val jaasCfg = String.format(jaasTemplate, USER, PASS)
  //  val GROUP = "demo"
  //  val deserializer = classOf[StringDeserializer].getName
  val S3_bucket = AppConfig("S3_bucket")
  println("Стартуем спарк сессию")

  def read_kfk_wrt_prqt(tmStmp: LocalDateTime) = {
    println("df - читает поток из топика кафки")
    val df = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", AppConfig("HOST"))
      .option("subscribe", AppConfig("TOPIC"))
//      .option(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest") // latest
//      .option(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
      .option("failOnDataLoss", "false")
      .option("auto.offset.reset", "earliest")
      //      .option("kafka.startingOffsets", "latest") //earliest  latest   only for first query
      //      .option("startingOffsets", """{"topic1":{"0":23,"1":-2},"topic2":{"0":-2}}""") // only for first query
      //      .option("endingOffsets", """{"topic1":{"0":50,"1":-1},"topic2":{"0":-1}}""") // only for batch
      .option("enable.auto.commit", "true")
      .option("kafka.group.id", AppConfig("GROUP"))
      .option(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, AppConfig("deserializer"))
      .option(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, AppConfig("deserializer"))
      .option("kafka.security.protocol", AppConfig("kafkaSecurityProtocol"))
      .option("kafka.sasl.mechanism", AppConfig("kafkaSaslMechanism"))
      .option("kafka.sasl.jaas.config", String.format(AppConfig("jaasTemplate"), AppConfig("USER"), AppConfig("PASS")))
      .option("kafka.ssl.truststore.location", AppConfig("TS_FILE"))
      .option("kafka.ssl.truststore.password", AppConfig("TS_PASS"))
      //      .option("fs.s3a.access.key", "92b77fsJhAuVmWtGReAT")
      //      .option("fs.s3a.secret.key", "FX6qvYjUTWwAHiz5BY45iikDbWZddn47Q9CFhyd8")
      //      .option("fs.s3a.endpoint", "storage.yandexcloud.net")
      .load()

    println("Определяем схему для value")
    val schema = new StructType()
      .add("@timestamp", StringType)
      .add("log", StringType)
      .add("stream", StringType)
      .add("time", TimestampType)

    println("Делаем каст колонок ")
    val personDF = df.select(col("key").cast("string"),
      (from_json(col("value").cast("string"), schema)
        .as("data")), col("partition"), col("offset"), col("timestamp"))
      .select("key", "data.*", "partition", "offset", "timestamp")

    println("Пишем в паркет S3" + s"s3a://$S3_bucket/s3-xxxx")

    personDF.writeStream
      .format("parquet").outputMode("append")
      .option("checkpointLocation", s"s3a://$S3_bucket/checkpnt_s3-${tmStmp}")
      .option("path", s"s3a://$S3_bucket/s3-${tmStmp}")
      .start()
      .awaitTermination(1000 * 23) //10 minutes
  }
}
