package KafkWriteParqS3

import java.time.LocalDateTime
import java.util.{Date, Timer, TimerTask}

object RepeatTimer {
  val kafkWriteParqS3 = new KafkWriteParqS3
  val timer = new Timer("Timer")
  val delay = 0
  val period: Int = 1000 * 30      //  * 60L * 24L

  def completeTask(): Unit = {
    var tmStmp = LocalDateTime.now()
    kafkWriteParqS3.read_kfk_wrt_prqt(tmStmp)
  }

  def scheduledTask(): Unit = {
    val repeatedTask = new TimerTask() {
      def run(): Unit = {
        println("Task performed on " + new Date())
        completeTask() }}
    timer.scheduleAtFixedRate(repeatedTask, delay, period)
  }

  def main(args: Array[String]): Unit = {
    scheduledTask()
  }
}
