package SparkReadS3

import SessionAndConnections.AppConfig

import scala.collection.JavaConversions._
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.auth.{AWSCredentials, AWSStaticCredentialsProvider}
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.model.{Bucket, ListObjectsRequest, ObjectListing}
import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}

import scala.collection.mutable.ListBuffer

class S3ObjectList {
  val s3_bucket: String = AppConfig("S3_bucket")
  val ls_S3_prefix = AppConfig("ls_S3_prefix")
  val credentials: AWSCredentials = new ProfileCredentialsProvider().getCredentials
  val s3: AmazonS3 = AmazonS3ClientBuilder.standard
    .withCredentials(new AWSStaticCredentialsProvider(credentials))
    .withEndpointConfiguration(new AwsClientBuilder
    .EndpointConfiguration("storage.yandexcloud.net", "ru-central1"))
    .build

  def s3ObjectsToList(): List[String] = {

    println(s"Listing objects with prefix s3 in $s3_bucket and replace parts***  and save to ListBuffer")
    val objectListing: ObjectListing = s3.listObjects(new ListObjectsRequest().withBucketName(s3_bucket)
      .withPrefix(ls_S3_prefix))
    val obj_buf = new ListBuffer[String]()
    for (objectSummary <- objectListing.getObjectSummaries) {
      obj_buf.+=(objectSummary.getKey.replaceAll("part-00.*", "")
        .replaceAll("_spark_metadata.*", "")
        .replaceAll("s3", s"s3a://$s3_bucket/s3"))
    }

    println("ListBuffer to List")
    val obj_list: List[String] = obj_buf.toList.distinct
    println("obj_list - ", obj_list)
    obj_list

  }
}
