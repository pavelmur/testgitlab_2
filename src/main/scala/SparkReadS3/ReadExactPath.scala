package SparkReadS3

import SessionAndConnections.SparkSess.spark

class ReadExactPath {
  println("Стартуем спарк сессию")

  def readParq() = {

    //    //    println("Запускаем скрипт - сохраняем пути бакетов S3 в txt файл")
    //    //    "src/main/resources/script_s3_paths_to_file.sh" !!
    //    //    println("Из файла сохраняем пути банкетов в лист и заменяем DIR_s3 на s3a, отфильтруем checkpnt")
    //    //    val lines = Source.fromFile("/Users/pmurzakov/IdeaProjects/Tchaykovsky_15nov/paths_222_333.txt").getLines.toList

    // ****   Вставить реально существующие пути к файлам паркет в S3" !!

    //    DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:21:34.284061/
    //      DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:33:09.418842/
    //    DIR  s3://test-gpm-data-30nov-2/s3-2021-11-30T18:33:43.514951/

    val df_1 = spark.read.parquet(s"s3a://test-gpm-data-4dec/union-parq/")
    //    val df_2 = spark.read.parquet(s"s3a://test-gpm-data-30nov-2/s3-2021-11-30T18:33:09.418842/")
    //    val df_3 = spark.read.parquet(s"s3a://test-gpm-data-30nov-2/s3-2021-11-30T18:33:43.514951/")
    //    val df_4 = spark.read.parquet(s"s3a://test-gpm-data-30nov/s3-2021-11-30T15:36:08.469689/")
    //    val df_5 = spark.read.parquet(s"s3a://test-gpm-data-30nov/s3-2021-11-30T15:56:14.635596/")


    val df_union = df_1 //.union(df_2).union(df_3)  //.union(df_4).union(df_5)
    val df_unon_distnct = df_union.distinct()  //.sort("offset")

    //    df_union.show(100)
    //    println("df_unon_distnct.show(200)")
    df_unon_distnct.show(200, false)
    println("df_union.count()", df_union.count())
    println("df_unon_distnct.count()", df_unon_distnct.count())
    df_union.printSchema()
//    df_union.describe("offset").show()

  }
}
