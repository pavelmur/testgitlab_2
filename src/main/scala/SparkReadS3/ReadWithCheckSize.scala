package SparkReadS3

import SessionAndConnections.AppConfig
import SessionAndConnections.SparkSess.spark
import com.amazonaws.services.s3.model.{ListObjectsRequest, ObjectListing}

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

class ReadWithCheckSize {

  val s3_bucket: String = AppConfig("S3_bucket")
  val buck_union_ext: String = AppConfig("buck_union_ext")
  val ls_S3_prefix = AppConfig("ls_S3_prefix")
  val s3ObjectList = new S3ObjectList

  def readS3_checkSize() = {
    var obj_buf_size = new ListBuffer[String]()
    var obj_buf_delete = new ListBuffer[Int]()
    var obj_buf = ListBuffer[String]()

    println("Listing objects with size and create List")
    val objectListing: ObjectListing = s3ObjectList.s3.listObjects(new ListObjectsRequest()
      .withBucketName(s3_bucket).withPrefix(ls_S3_prefix))

    for (objectSummary <- objectListing.getObjectSummaries) {
      obj_buf_size.+=(objectSummary.getKey + "  " + "size = " + objectSummary.getSize)
    }

    println("obj_buf_size - ", obj_buf_size)
    println("Фильтруем записи содержащие /part-00000")
    val obj_buf_size_filtr = obj_buf_size.filter(_.contains("/part-00000"))

    println("Удаляем содержащие size 900 или size 0")
    val nn = obj_buf_size_filtr.length
    for (n <- 0 until (nn))
      if (obj_buf_size_filtr(n).contains("size = 900")
        || obj_buf_size_filtr(n).contains("size = 0")
        || obj_buf_size_filtr(n).contains("size = 884"))
        obj_buf_delete.+=(n)

    for (i <- (obj_buf_size_filtr.size - 1) to 0 by -1)
      if (obj_buf_delete contains i) obj_buf_size_filtr remove i

    println("obj_buf_delete - ", obj_buf_delete)
    println()
    println("obj_buf_size_filtr - ", obj_buf_size_filtr)
    println()

    for (y <- obj_buf_size_filtr) {
      obj_buf.+=(y.replaceAll("  size = .*", "")
        .replaceAll("s3", s"s3a://$s3_bucket/s3"))
    }


    println("ListBuffer to List")
    val obj_list: List[String] = obj_buf.toList.distinct
    //    obj_list
    println(obj_list)

    println("В цикле читаем все паркеты из сохранённых путей S3 и записываем в Другой Bucket")
    val ii: Int = obj_list.length

    for (i <- 0 until (ii)) {
      spark.read.parquet(obj_list(i))
        .write.mode("append").parquet(s"s3a://$s3_bucket/$buck_union_ext/")
    }

    val df_all = spark.read.parquet(s"s3a://$s3_bucket/$buck_union_ext/").distinct()
    df_all.show()
    println("df_all.count() - ", df_all.count())

  }
}
