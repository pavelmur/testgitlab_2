import org.apache.logging.log4j.core.config.composite.MergeStrategy
import sun.security.tools.PathList

name := "Tchaykovsky_15nov"

version := "0.1"

scalaVersion := "2.12.15"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "3.2.0",
  "org.apache.spark" %% "spark-sql" % "3.2.0",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % "3.2.0",
  "org.apache.kafka" % "kafka-clients" % "3.0.0",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.12.1",
  "org.slf4j" % "slf4j-simple" % "2.0.0-alpha5",
  "org.apache.hadoop" % "hadoop-common" % "3.3.1",
  "org.apache.hadoop" % "hadoop-client" % "3.3.1",
//  "com.amazonaws" % "aws-java-sdk-bom" % "1.11.1000" pomOnly(),
//  "com.amazonaws" % "aws-java-sdk-s3" % "1.11.1000",
  "com.amazonaws" % "aws-java-sdk" % "1.12.106",
//  "com.amazonaws" % "aws-java-sdk-core" % "1.12.106",
//  "com.amazonaws" % "aws-java-sdk-dynamodb" % "1.12.106",
//  "com.amazonaws" % "aws-java-sdk-kms" % "1.12.106",
//  "com.amazonaws" % "aws-java-sdk-s3" % "1.12.106",
  "org.apache.httpcomponents" % "httpclient" % "4.5.13",
  "org.apache.httpcomponents" % "httpcore" % "4.4.13",
  "joda-time" % "joda-time" % "2.10.13",
  "org.apache.hadoop" % "hadoop-aws" % "3.3.1",
  "net.java.dev.jets3t" % "jets3t" % "0.9.4",
  "log4j" % "log4j" % "1.2.17",
)

//// META-INF discarding
//assemblyMergeStrategy in assembly := {
//  case PathList("META-INF","services",xs @ _*) => MergeStrategy.filterDistinctLines
//  case PathList("META-INF",xs @ _*) => MergeStrategy.discard
//  case "application.conf" => MergeStrategy.concat
//  case _ => MergeStrategy.first
//}
